﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Kakytubata.Windows.WPFExtensions
{
    public class FileImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Uri GetNoArt() => new Uri("/Resources/NoAlbumArt.png", UriKind.Relative);
            if (value == null) return GetNoArt();
            var str = value as string;
            if (string.IsNullOrEmpty(str)) return GetNoArt();
            if (string.IsNullOrWhiteSpace(str)) return GetNoArt();
            return new Uri(str);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
