﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;

namespace Kakytubata.Windows.WPFExtensions
{
    public class GridLengthPixelAnimation : AnimationTimeline
    {
        public override Type TargetPropertyType => typeof(GridLength);

        protected override Freezable CreateInstanceCore() => new GridLengthPixelAnimation();

        static GridLengthPixelAnimation()
        {
            FromProperty = DependencyProperty.Register("From", typeof(double),
                typeof(GridLengthPixelAnimation));

            ToProperty = DependencyProperty.Register("To", typeof(double),
                typeof(GridLengthPixelAnimation));

            EasingFunctionProperty = DependencyProperty.Register("EasingFunction", typeof(EasingFunctionBase),
                typeof(GridLengthPixelAnimation));
        }
        public static readonly DependencyProperty FromProperty;
        public double From {
            get {
                return (double)GetValue(GridLengthPixelAnimation.FromProperty);
            }
            set {
                SetValue(GridLengthPixelAnimation.FromProperty, value);
            }
        }
        public static readonly DependencyProperty ToProperty;
        public double To {
            get {
                return (double)GetValue(GridLengthPixelAnimation.ToProperty);
            }
            set {
                SetValue(GridLengthPixelAnimation.ToProperty, value);
            }
        }

        public static readonly DependencyProperty EasingFunctionProperty;
        public EasingFunctionBase EasingFunction {
            get => (EasingFunctionBase)GetValue(GridLengthPixelAnimation.EasingFunctionProperty);
            set => SetValue(GridLengthPixelAnimation.EasingFunctionProperty, value);
        }

        public override object GetCurrentValue(object defaultOriginValue,
                object defaultDestinationValue, AnimationClock animationClock)
        {
            double fromVal = From;
            double toVal = To;
            EasingFunctionBase ease = GetValue(GridLengthPixelAnimation.EasingFunctionProperty) as EasingFunctionBase;

            if (ease == null)
            {
                if (fromVal > toVal)
                {
                    return new GridLength(((1.0 - animationClock.CurrentProgress.Value) *
                        (fromVal - toVal)) + toVal, GridUnitType.Pixel);
                }
                else
                {
                    return new GridLength(animationClock.CurrentProgress.Value *
                        (toVal - fromVal) + fromVal, GridUnitType.Pixel);
                }
            }
            else
            {
                if (fromVal > toVal)
                {
                    return new GridLength(((1.0 - ease.Ease(animationClock.CurrentProgress.Value)) *
                        (fromVal - toVal)) + toVal, GridUnitType.Pixel);
                }
                else
                {
                    return new GridLength(ease.Ease(animationClock.CurrentProgress.Value) *
                        (toVal - fromVal) + fromVal, GridUnitType.Pixel);
                }
            }
        }
    }
}
