﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Kakytubata.Windows.WPFExtensions
{
    /// <summary>
    /// 使いまわすと速度落ちるよー
    /// DO NOT RECYCLE.(this will be slow.)
    /// </summary>
    public class PointCollectionAnimation: AnimationTimeline
    {
        public override Type TargetPropertyType => typeof(PointCollection);

        protected override Freezable CreateInstanceCore() => new PointCollectionAnimation();

        static PointCollectionAnimation()
        {
            FromProperty = DependencyProperty.Register("From", typeof(PointCollection),
                typeof(PointCollectionAnimation));
            ToProperty = DependencyProperty.Register("To", typeof(PointCollection),
                            typeof(PointCollectionAnimation));
            EasingFunctionProperty = DependencyProperty.Register("EasingFunction", typeof(EasingFunctionBase),
                            typeof(PointCollectionAnimation));
        }

        public static readonly DependencyProperty FromProperty;
        public PointCollection From {
            get => (PointCollection)GetValue(PointCollectionAnimation.FromProperty);
            set {
                PointCollection to = GetValue(PointCollectionAnimation.ToProperty) as PointCollection;
                if (to == null)
                    SetValue(PointCollectionAnimation.FromProperty, value);
                else
                {
                    if (to.Count == value.Count)
                    {
                        SetValue(PointCollectionAnimation.FromProperty, value);
                    }
                    else if (to.Count > value.Count)
                    {
                        for (int i = value.Count; i < to.Count; ++i)
                            value.Add(value[value.Count - 1]);
                        SetValue(PointCollectionAnimation.FromProperty, value);
                    }
                    else
                    {
                        for (int i = to.Count; i < value.Count; ++i)
                            to.Add(to[to.Count - 1]);
                        SetValue(PointCollectionAnimation.FromProperty, value);
                    }
                }
            }
        }

        public static readonly DependencyProperty ToProperty;
        public PointCollection To {
            get => (PointCollection)GetValue(PointCollectionAnimation.ToProperty);
            set {
                PointCollection from = GetValue(PointCollectionAnimation.FromProperty) as PointCollection;
                if (from == null)
                    SetValue(PointCollectionAnimation.ToProperty, value);
                else
                {
                    if(from.Count == value.Count)
                    {
                        SetValue(PointCollectionAnimation.ToProperty, value);
                    }
                    else if(from.Count > value.Count)
                    {
                        for (int i = value.Count; i < from.Count; ++i)
                            value.Add(value[value.Count - 1]);
                        SetValue(PointCollectionAnimation.ToProperty, value);
                    }
                    else
                    {
                        for (int i = from.Count; i < value.Count; ++i)
                            from.Add(from[from.Count - 1]);
                        SetValue(PointCollectionAnimation.ToProperty, value);
                    }
                }
            }
        }

        private PointCollection Cache = null;

        public static readonly DependencyProperty EasingFunctionProperty;
        public EasingFunctionBase EasingFunction {
            get => (EasingFunctionBase)GetValue(PointCollectionAnimation.EasingFunctionProperty);
            set => SetValue(PointCollectionAnimation.EasingFunctionProperty, value);
        }

        public override object GetCurrentValue(object defaultOriginValue, object defaultDestinationValue, AnimationClock animationClock)
        {
            PointCollection from = From;
            PointCollection to = To;
            EasingFunctionBase ease = GetValue(PointCollectionAnimation.EasingFunctionProperty) as EasingFunctionBase;

            if (Cache == null)
            {
                Cache = new PointCollection(from.Count);
                for (int i = 0; i < from.Count; ++i)
                    Cache.Add(new Point());
            }
            if(ease == null)
            {
                for(int i = 0; i < from.Count; ++i)
                {
                    Point fromVal = from[i];
                    Point toVal = to[i];
                    Point result = new Point();
                    if (fromVal.X > toVal.X)
                        result.X = ((1.0 - animationClock.CurrentProgress.Value) * (fromVal.X - toVal.X)) + toVal.X;
                    else
                        result.X = animationClock.CurrentProgress.Value * (toVal.X - fromVal.X) + fromVal.X;
                    if (fromVal.Y > toVal.Y)
                        result.Y = ((1.0 - animationClock.CurrentProgress.Value) * (fromVal.Y - toVal.Y)) + toVal.Y;
                    else
                        result.Y = animationClock.CurrentProgress.Value * (toVal.Y - fromVal.Y) + fromVal.Y;
                    Cache[i] = result;
                }
            }
            else
            {
                for (int i = 0; i < from.Count; ++i)
                {
                    Point fromVal = from[i];
                    Point toVal = to[i];
                    Point result = new Point();
                    if (fromVal.X > toVal.X)
                        result.X = ((1.0 - ease.Ease(animationClock.CurrentProgress.Value)) * (fromVal.X - toVal.X)) + toVal.X;
                    else
                        result.X = ease.Ease(animationClock.CurrentProgress.Value) * (toVal.X - fromVal.X) + fromVal.X;
                    if (fromVal.Y > toVal.Y)
                        result.Y = ((1.0 - ease.Ease(animationClock.CurrentProgress.Value)) * (fromVal.Y - toVal.Y)) + toVal.Y;
                    else
                        result.Y = ease.Ease(animationClock.CurrentProgress.Value) * (toVal.Y - fromVal.Y) + fromVal.Y;
                    Cache[i] = result;
                }
            }
            return Cache;
        }
    }
}
