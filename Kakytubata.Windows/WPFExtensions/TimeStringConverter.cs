﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Kakytubata.Windows.WPFExtensions
{
    public class TimeStringConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TimeSpan unboxed = (TimeSpan)value;
            return unboxed.TotalHours > 0 ? $"{unboxed.TotalHours}:{unboxed.Minutes}:{unboxed.Seconds}" : $"{unboxed.Minutes}:{unboxed.Seconds}";
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
