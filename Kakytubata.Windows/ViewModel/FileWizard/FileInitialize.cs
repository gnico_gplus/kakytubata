﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Kakytubata.Windows.ViewModel.FileWizard
{
    public class FileInitialize : MVVM.PropertyChangedBase
    { 
        public State.State State { get; set; }
        public Library.FileInfo CurrentFile { get; set; }

        private string imageFilePath = "";
        public string ImageFilePath {
            get => imageFilePath;
            set {
                imageFilePath = value;
                Notify();
            }
        }

        public FileInitialize(State.State state)
        {
            State = state;
            CurrentFile = new Library.FileInfo(state.CurrentLibrary) { Data = Library.File.CreateEmpty() };
        }

        public void Save()
        {
            string BaseDirectoryPath = CurrentFile.InitializeBaseDirectoryName();
            if (BaseDirectoryPath == null) throw new Exception();
            CurrentFile.InitializeDirectory();
            if (string.IsNullOrEmpty(imageFilePath) || string.IsNullOrWhiteSpace(imageFilePath))
            { }
            else
            {
                string fileExtension = Path.GetExtension(imageFilePath);
                string finalArtFileName = Library.File.DefaultArtFileName + fileExtension;
                string finalArtFilePath = Path.Combine(BaseDirectoryPath, finalArtFileName);
                File.Copy(imageFilePath, finalArtFilePath);
                CurrentFile.Data.ImageFileName = finalArtFileName;
            }
            CurrentFile.Save();
            State.CurrentLibrary.Files.Add(CurrentFile);
        }
    }
}
