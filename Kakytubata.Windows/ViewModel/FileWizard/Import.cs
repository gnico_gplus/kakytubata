﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using EmergenceGuardian.FFmpeg;
using Reactive.Bindings;
using static Kakytubata.Library.MediaCodec;

namespace Kakytubata.Windows.ViewModel.FileWizard
{
    public class Import : MVVM.PropertyChangedBase
    {
        public State.State State { get; private set; }
        public Library.FileInfo CurrentFile { get; private set; }
        public ObservableCollection<ImportFile> Imports { get; set; } = new ObservableCollection<ImportFile>();
        public FFmpegProcess CurrrentFFmpegProcess { get; set; }

        private volatile bool IsContinue = true;

        public ReactiveProperty<bool> IsCompleted = new ReactiveProperty<bool>(false);

        public Import(Library.FileInfo file, State.State state)
        {
            CurrentFile = file;
            State = state;
        }

        public void EndInit()
        {
            StopLoop();
            for (int i = 0; i < Imports.Count; ++i)
                CurrentFile.Data.Media.Add(new Library.MediaInfo() { FileName = Path.GetFileName(Imports[i].Destination) });
            CurrentFile.Save();
        }

        public void StopLoop() => IsContinue = false;

        public Task StartLoop(Dispatcher dispatch)
        {
            return Task.Run(() =>
            {
                while(IsContinue)
                {
                    if (IsCompleted.Value)
                        Thread.Sleep(1000);
                    else
                        StartNext(dispatch);
                }
            });
        }

        public void StartNext(Dispatcher dispatch) {
            ImportFile next = null;
            for (int i = 0; i < Imports.Count; ++i)
                if (!(Imports[i].IsCompleted | Imports[i].IsError)) next = Imports[i];
            if(next == null)
            {
                IsCompleted.Value = true;
                return;
            }

            var option = new ProcessStartOptions();
            option.DisplayMode = FFmpegDisplayMode.None;
            option.Started += (ss, ee) =>
                ee.Process.StatusUpdated += (sss, eee) =>
                 {
                     double value = (double)eee.Status.Frame / ee.Process.FrameCount;
                     System.Diagnostics.Debug.WriteLine(value);
                     dispatch.BeginInvoke((Action)(() => next.Progress = value));
                 };
            var codecInfo = State.CurrentLibrary.Compressor;
            CompletionStatus result;
            if (codecInfo.GetAttribute<CodecNameAttribute>().Name == "flac")
                result = MediaEncoder.Encode(next.Source, null, "flac", null, next.Destination, option);
            else
                result = MediaEncoder.Encode(next.Source, null, "libopus", $"-b:a {codecInfo.GetAttribute<FileSizeAttribute>().Kbps}k -application audio", next.Destination, option);
            switch(result)
            {
                case CompletionStatus.Success:
                    dispatch.BeginInvoke((Action)(() => next.IsCompleted = true));
                    break;
                default:
                    dispatch.BeginInvoke((Action)(() => {
                            next.IsCompleted = false;
                            next.IsError = true;
                        }));
                    break;
            }
        }

        public void AddFile(string path)
        {
            string outFileName = Path.GetFileNameWithoutExtension(path);
            string outFilePath = Path.Combine(CurrentFile.FullPath, outFileName + ".mka");
            Imports.Add(new ImportFile(path, outFilePath));
            IsCompleted.Value = false;
        }
    }

    public class ImportFile : MVVM.PropertyChangedBase
    {
        public string FileName { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }

        private double progress = 0.0f;
        public double Progress {
            get => progress;
            set {
                progress = value;
                Notify();
            }
        }

        private bool _isCompleted = false;
        public bool IsCompleted {
            get => _isCompleted;
            set {
                _isCompleted = value;
                Notify();
            }
        }

        private bool _isError = false;
        public bool IsError {
            get => _isError;
            set {
                _isError = value;
                Notify();
            }
        }

        public ImportFile(string src, string dst)
        {
            Source = src;
            Destination = dst;
            FileName = Path.GetFileNameWithoutExtension(src);
        }
    }
}
