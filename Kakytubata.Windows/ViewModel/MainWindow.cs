﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reactive.Bindings;

namespace Kakytubata.Windows.ViewModel
{
    public class MainWindow
    {
        public ReactiveProperty<double> MediaPosition_Slider { get; } = new ReactiveProperty<double>(0);
        public ReactiveProperty<bool?> IsPlaying { get; } = new ReactiveProperty<bool?>(false);
        public ReactiveProperty<double> Volume { get; } = new ReactiveProperty<double>(60);
        public ReactiveProperty<TimeSpan> MediaLength { get; } = new ReactiveProperty<TimeSpan>(TimeSpan.FromTicks(0));
        public ReactiveProperty<TimeSpan> MediaPosition { get; } = new ReactiveProperty<TimeSpan>(TimeSpan.FromTicks(0));

        public ReactiveProperty<bool> NextSongMovable { get; } = new ReactiveProperty<bool>(false);

        private Vlc.DotNet.Core.VlcMediaPlayer CurrentPlayer = null;

        private volatile bool isSeekBarUserControlling = false;

        public State.Playlist Playlist { get; } = new State.Playlist();

        public State.PlayState state { get; set; }

        public void Initialize(Windows.MainWindow parent, State.State currentState)
        {
            state = new State.PlayState(currentState, parent.MainPlayer, parent.Dispatcher, Playlist);

            CurrentPlayer = parent.MainPlayer.SourceProvider.MediaPlayer;
            CurrentPlayer.Audio.Volume = 60;

            parent.SeekBar.PreviewMouseLeftButtonDown += (ss, ee) =>
            {
                isSeekBarUserControlling = true;
                CurrentPlayer.Pause();
            };
            parent.SeekBar.PreviewMouseLeftButtonUp += (ss, ee) =>
            {
                isSeekBarUserControlling = false;
                CurrentPlayer.Position = (float)MediaPosition_Slider.Value;
                CurrentPlayer.Play();
            };

            parent.PlayPauseButton.Click += (ss, ee) =>
            {
                if (parent.PlayPauseButton.IsChecked == true)
                    CurrentPlayer.Play();
                else
                    CurrentPlayer.Pause();
            };
            parent.BackToHeadButton.Click += (ss, ee) =>
            {
                if (CurrentPlayer.IsSeekable)
                    CurrentPlayer.Position = 0;
            };
            parent.NextSongButton.Click += (ss, ee) =>
            {
                state.GotoNext();
            };

            Volume.Subscribe(d => CurrentPlayer.Audio.Volume = (int)d);

            CurrentPlayer.PositionChanged += (ss, ee) =>
            {
                if (!isSeekBarUserControlling)
                {
                    MediaPosition_Slider.Value = ee.NewPosition;
                    MediaPosition.Value = TimeSpan.FromTicks((long)(ee.NewPosition * MediaLength.Value.Ticks));
                }
            };
            CurrentPlayer.Playing += (ss, ee) =>
            {
                IsPlaying.Value = true;
            };
            CurrentPlayer.Paused += (ss, ee) =>
            {
                IsPlaying.Value = isSeekBarUserControlling;
            };
            CurrentPlayer.MediaChanged += (ss, ee) =>
            {
                var media = ee.NewMedia;
                MediaLength.Value = media.Duration;
                NextSongMovable.Value = state.MediaList.IsIncrementable;
            };

            state.MediaList.CollectionChanged += (ss, ee) =>
            {
                NextSongMovable.Value = state.MediaList.IsIncrementable;
            };
        }
    }
}
