﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kakytubata.Windows.ViewModel.LibraryWizard
{
    public class CreateLibrary
    {
        public Library.Library Data { get; }
        public FileTypeItem[] FileTypes { get; }
        
        public CreateLibrary()
        {
            Data = new Library.Library();
            FileTypes = new FileTypeItem[]
                {
                    new FileTypeItem("Lossless (FLAC)", Library.MediaCodec.FileType.FLAC),
                    new FileTypeItem("Best (Opus 256kbps)", Library.MediaCodec.FileType.Opus256),
                    new FileTypeItem("Good (Opus 128kbps)", Library.MediaCodec.FileType.Opus128),
                    new FileTypeItem("Normal (Opus 96kbps)", Library.MediaCodec.FileType.Opus96),
                    new FileTypeItem("Highly Compressed (Opus 24kbps)", Library.MediaCodec.FileType.Opus24)
                };
        }

        public class FileTypeItem
        {
            public string DisplayName { get; set; }
            public Library.MediaCodec.FileType Value { get; set; }

            public FileTypeItem(string name, Library.MediaCodec.FileType value)
            {
                DisplayName = name;
                Value = value;
            }
        }
    }
}
