﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kakytubata.Windows.ViewModel.MainWindowPages
{
    public class FilePage
    {
        public Library.FileInfo CurrentFile { get; set; }

        public FilePage(Library.FileInfo currentFile)
        {
            CurrentFile = currentFile;
        }

    }
}
