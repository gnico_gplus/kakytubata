﻿using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Kakytubata.Windows.ViewModel.MainWindowPages
{
    public class MediaPage : IDisposable
    {
        public ReactiveProperty<string> MediaPath { get; }
        private TagLib.File File;

        private bool isModifying = false;

        public ReactiveProperty<BitmapSource> FrontCover { get; } = new ReactiveProperty<BitmapSource>(new BitmapImage(new Uri("/Resources/NoAlbumArt.png", UriKind.Relative)));

        public ReactiveProperty<string> Title { get; } = new ReactiveProperty<string>();
        public ReactiveProperty<string> Year { get; } = new ReactiveProperty<string>();
        public ReactiveProperty<string> Creator { get; } = new ReactiveProperty<string>();

        public MediaPage(string mediaPath, TagLib.File file = null)
        {
            MediaPath = new ReactiveProperty<string>(mediaPath);
            File = file;
        }

        public async Task BeginLoad()
        {
            var (f, observable) = File == null ? await Library.MediaTag.OpenFileWithPicture(MediaPath.Value) : await Library.MediaTag.OpenPictures(File);
            observable.Subscribe(onNext: res =>
            {
                var (t, bmp) = res;
                if(t == TagLib.PictureType.FrontCover)
                {
                    FrontCover.Value = bmp;
                }
            }, onCompleted: () => {

            });
            File = f;
            TagLib.Tag tag = f.Tag;
            TagLib.Matroska.Tag hoge = new TagLib.Matroska.Tag();
            Title.Value = tag.Title ?? "No Title";
            Year.Value = tag.Year == 0 ? "No Date" : tag.Year.ToString();
            Creator.Value = tag.Performers == null || tag.Performers.Length == 0  || tag.Performers[0] == null ? "No Creators" : String.Join(",", tag.Performers);
        }


        public void Dispose()
        {
            if(!isModifying)
                File?.Dispose();
        }
    }
}
