﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Windows {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public ViewModel.MainWindow Data { get; set; } = new ViewModel.MainWindow();

        private State.State __state;

        public MainWindow(State.State state) {
            __state = state;
            DataContext = Data;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Data.Initialize(this, __state);
            MainFrame.Navigate(new Pages.MainWindow.LibraryPage() { Parent = this, State = Data.state });
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Data.state.Dispose();
            Environment.Exit(0);
        }
    }
}
