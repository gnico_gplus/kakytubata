﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Windows
{
    /// <summary>
    /// LibraryWizardWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class LibraryWizardWindow : Window
    {
        Pages.LibraryWizard.LibraryWizardPageBase Current { get; set; }

        public LibraryWizardWindow(State.State state, bool IsShowWelcome = false)
        {
            Current = IsShowWelcome ? (Pages.LibraryWizard.LibraryWizardPageBase)(new Pages.LibraryWizard.Welcome() { State = state, Parent = this}) : new Pages.LibraryWizard.CreateOrImportLibrary() { State = state, Parent = this };
            InitializeComponent();
            MainFrame.Navigate(Current);
        }

        private async void NextButton_Click(object sender, RoutedEventArgs e)
        {
            Current = await Current.GetNextPage();
            if (Current == null) return;
            MainFrame.Navigate(Current);
            BackButton.IsEnabled = MainFrame.CanGoBack;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.GoBack();
            BackButton.IsEnabled = MainFrame.CanGoBack;
            NextButton.IsEnabled = true;
        }
    }
}
