﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Windows
{
    /// <summary>
    /// FileWizardWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class FileWizardWindow : Window
    {
        Pages.FileWizard.FileWizardPageBase Current { get; set; }
        State.State State { get; set; }

        public FileWizardWindow()
        {
            InitializeComponent();
        }

        public FileWizardWindow(State.State state)
        {
            InitializeComponent();
            State = state;
            Current = new Pages.FileWizard.CreateFile() { Parent = this, State = State};
            MainFrame.Navigate(Current);
        }

        private async void NextButton_Click(object sender, RoutedEventArgs e)
        {
            Current = await Current.GetNextPage();
            if (Current == null) return;
            MainFrame.Navigate(Current);
            BackButton.IsEnabled = MainFrame.CanGoBack;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.GoBack();
            BackButton.IsEnabled = MainFrame.CanGoBack;
            NextButton.IsEnabled = true;
        }
    }
}
