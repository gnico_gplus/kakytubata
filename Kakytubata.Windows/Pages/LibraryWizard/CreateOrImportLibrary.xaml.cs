﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Pages.LibraryWizard
{
    /// <summary>
    /// CreateOrImportLibrary.xaml の相互作用ロジック
    /// </summary>
    public partial class CreateOrImportLibrary : LibraryWizardPageBase
    {
        public CreateOrImportLibrary()
        {
            InitializeComponent();
        }

        private void WizardPageBase_Loaded(object sender, RoutedEventArgs e)
        {
            Parent.NextButton.IsEnabled = false;
        }

#pragma warning disable CS1998
        public override async Task<LibraryWizardPageBase> GetNextPage()
        {
            if (CreateRadioButton.IsChecked == true) return new CreateLibrary(){ State = this.State, Parent = this.Parent};
            if (ImportRadioButton.IsChecked == true) return new ImportLibrary() { State = this.State, Parent = this.Parent };
            return null;
        }
#pragma warning restore CS1998

        private void CreateRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            Parent.NextButton.IsEnabled = true;
        }
        private void ImportRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            Parent.NextButton.IsEnabled = true;
        }
    }
}
