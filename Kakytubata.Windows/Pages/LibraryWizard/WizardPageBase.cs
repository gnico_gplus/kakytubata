﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Kakytubata.Windows.Pages.LibraryWizard
{
    public abstract class LibraryWizardPageBase : Page
    {
        public State.State State { get; set; }
        public new Windows.LibraryWizardWindow Parent { get; set; }

        protected LibraryWizardPageBase Copy(LibraryWizardPageBase page)
        {
            page.Parent = this.Parent;
            page.State = this.State;
            return page;
        }

        public abstract Task<LibraryWizardPageBase> GetNextPage();
    }
}
