﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Kakytubata.Windows.Pages.LibraryWizard
{
    /// <summary>
    /// Library.xaml の相互作用ロジック
    /// </summary>
    public partial class CreateLibrary : LibraryWizardPageBase
    {
        ViewModel.LibraryWizard.CreateLibrary DataContext_Typed { get; set; }

        public CreateLibrary()
        {
            InitializeComponent();
        }

        private void WizardPageBase_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext_Typed = new ViewModel.LibraryWizard.CreateLibrary();
            DataContext = DataContext_Typed;
            Parent.NextButton.IsEnabled = false;
        }

        public override async Task<LibraryWizardPageBase> GetNextPage()
        {
            State.Setting.LibrariesPath.Add(DataContext_Typed.Data.LibraryPath);
            await State.Setting.SaveAsync();
            await DataContext_Typed.Data.InitializeDirectoryAsync();
            State.CurrentLibrary = DataContext_Typed.Data;
            Windows.MainWindow mw = new Windows.MainWindow(State);
            App.Current.MainWindow = mw;
            mw.Show();
            Parent.Close();
            return null;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Parent.NextButton.IsEnabled = !string.IsNullOrEmpty(PathTextBox.Text) && !string.IsNullOrEmpty(TitleTextBox.Text);
        }

        private void ReferenceButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Library Location";
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            sfd.Filter = "Kakytubata Library File(*.kakytubatalibrary)|*.kakytubatalibrary";
            sfd.FilterIndex = 1;
            if(sfd.ShowDialog() == true)
            {
                DataContext_Typed.Data.LibraryPath = sfd.FileName;
                Parent.NextButton.IsEnabled = !string.IsNullOrEmpty(PathTextBox.Text) && !string.IsNullOrEmpty(TitleTextBox.Text);
            }
        }
    }
}
