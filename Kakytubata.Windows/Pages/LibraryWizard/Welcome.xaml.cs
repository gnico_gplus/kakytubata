﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Pages.LibraryWizard
{
    /// <summary>
    /// Welcome.xaml の相互作用ロジック
    /// </summary>
    public partial class Welcome : LibraryWizardPageBase
    {
        public Welcome()
        {
            InitializeComponent();
        }

#pragma warning disable CS1998
        public override async Task<LibraryWizardPageBase> GetNextPage()
          => new CreateOrImportLibrary() { State = State, Parent = Parent };
#pragma warning restore CS1998
    }
}
