﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Kakytubata.Windows.Pages.MainWindow
{
    /// <summary>
    /// FilePage.xaml の相互作用ロジック
    /// </summary>
    public partial class FilePage : MainWindowPageBase
    {
        public ViewModel.MainWindowPages.FilePage Data { get; set; }

        (Task task, CancellationTokenSource canceller) mediaInfoLoader { get; set; }

        Library.FileInfo ___fileInfo;


        public FilePage()
        {
            InitializeComponent();
        }

        public FilePage(Library.FileInfo currentFile)
        {
            InitializeComponent();
            ___fileInfo = currentFile;
        }

        private void MainWindowPageBase_Loaded(object sender, RoutedEventArgs e)
        {
            Data = new ViewModel.MainWindowPages.FilePage(___fileInfo);

            DataContext = Data;

            Data.CurrentFile.SetParentPage(this);
            mediaInfoLoader = Data.CurrentFile.StartMediaLoad(Dispatcher);
        }

        private void MainWindowPageBase_Unloaded(object sender, RoutedEventArgs e)
        {
            if (mediaInfoLoader.task == null) return;
            if (!mediaInfoLoader.task.IsCompleted)
                mediaInfoLoader.canceller.Cancel();
            Data.CurrentFile.ReleaseParentPage();
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            base.ParentBack();
        }

        private void PlayAllButton_Click(object sender, RoutedEventArgs e)
        {
            for(int i = 0; i < Data.CurrentFile.Data.Media.Count; ++i)
            {
                var m = Data.CurrentFile.Data.Media[i];
                State.AddPlaylist(Path.Combine(Data.CurrentFile.FullPath, m.FileName));
            }
        }

        private void MediaListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var selected = MediaListBox.SelectedItem;
            if (selected == null) return;
            var _listBoxItem = MediaListBox.ItemContainerGenerator.ContainerFromItem(selected);
            if (_listBoxItem == null) return;
            var listBoxItem = _listBoxItem as ListBoxItem;
            if (listBoxItem == null) return;
            if(listBoxItem.InputHitTest(e.GetPosition(listBoxItem)) != null)
            {
                var data = selected as Library.MediaInfo;
                if (data == null) return;
                base.ParentNavigate(new MediaPage(Path.Combine(Data.CurrentFile.FullPath, data.FileName), data.Data));
            }
            
        }
    }
}
