﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Kakytubata.Windows.Pages.MainWindow
{
    public class MainWindowPageBase : Page
    {
        public State.PlayState State { get; set; }
        public new Windows.MainWindow Parent { get; set; }

        protected void ParentNavigate(MainWindowPageBase page)
        {
            page.Parent = this.Parent;
            page.State = this.State;
            Parent.MainFrame.Navigate(page);
        }

        protected  void ParentBack()
        {
            Parent.MainFrame.GoBack();
        }
    }
}
