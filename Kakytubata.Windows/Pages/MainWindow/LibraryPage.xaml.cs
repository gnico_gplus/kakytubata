﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Pages.MainWindow
{
    /// <summary>
    /// LibraryPage.xaml の相互作用ロジック
    /// </summary>
    public partial class LibraryPage : MainWindowPageBase
    {
        private bool IsAddFileShowing { get; set; } = false;

        public LibraryPage()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            State.CurrentLibrary.BeginFilesLoad(this.Dispatcher);
            DataContext = State;
        }

        private void AddFile_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddFileShowing) return;
            IsAddFileShowing = true;
            var window = new Windows.FileWizardWindow(State);
            window.Closed += (ss, ee) => IsAddFileShowing = false;
            window.Show();
        }

        private void FilesListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var data = FilesListBox.SelectedItem as Library.FileInfo;
            if (data == null) return;
            var _listBoxItem = FilesListBox.ItemContainerGenerator.ContainerFromItem(data);
            if (_listBoxItem == null) return;
            var listBoxItem = _listBoxItem as ListBoxItem;
            if (listBoxItem == null) return;
            if(listBoxItem.InputHitTest(e.GetPosition(listBoxItem)) != null)
                this.ParentNavigate(new FilePage(data));
        }
    }
}
