﻿using Reactive.Bindings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Pages.MainWindow
{
    /// <summary>
    /// MediaPage.xaml の相互作用ロジック
    /// </summary>
    public partial class MediaPage : MainWindowPageBase
    {
        public ViewModel.MainWindowPages.MediaPage Data;

        public MediaPage(string mediaPath, TagLib.File file = null)
        {
            Data = new ViewModel.MainWindowPages.MediaPage(mediaPath, file);
            InitializeComponent();
            DataContext = Data;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await Data.BeginLoad();
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
            Data.Dispose();
        }

        private void AddToPlayListButton_Clicked(object sender, RoutedEventArgs e)
        {
            State.AddPlaylist(Data.MediaPath.Value);
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            base.ParentBack();
        }
    }

}
