﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Pages.FileWizard
{
    /// <summary>
    /// Import.xaml の相互作用ロジック
    /// </summary>
    public partial class Import : FileWizardPageBase
    {
        ViewModel.FileWizard.Import DataContext_Typed { get; set; }
        Library.FileInfo ___fileInfo;
        public Import()
        {
            InitializeComponent();
        }

        public Import(Library.FileInfo currentFileInfo)
        {
            InitializeComponent();
            ___fileInfo = currentFileInfo;
        }

#pragma warning disable CS1998
        public override async Task<FileWizardPageBase> GetNextPage()
        {
            DataContext_Typed.EndInit();
            Parent.Close();
            return null;
        }
#pragma warning restore CS1998

        private void FileWizardPageBase_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext_Typed = new ViewModel.FileWizard.Import(___fileInfo, State);
            DataContext = DataContext_Typed;
            DataContext_Typed.StartLoop(Dispatcher);
            DataContext_Typed.IsCompleted.Subscribe(onNext: (b) => Dispatcher.Invoke((Action)(() => Parent.NextButton.IsEnabled = b)));
        }

        private void ListBox_Drop(object sender, DragEventArgs e)
        {
            string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
            if (files == null) return;
            foreach (var f in files)
                DataContext_Typed.AddFile(f);
        }

        private void ListBox_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
                e.Effects = DragDropEffects.Copy;
            else
                e.Effects = DragDropEffects.None;
            e.Handled = true;
        }
    }
}
