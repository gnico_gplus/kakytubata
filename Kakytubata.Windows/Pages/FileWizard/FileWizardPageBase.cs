﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Kakytubata.Windows.Pages.FileWizard
{
    public abstract class FileWizardPageBase : Page
    {
        public State.State State { get; set; }
        public new Windows.FileWizardWindow Parent { get; set; }

        protected FileWizardPageBase Copy(FileWizardPageBase page)
        {
            page.Parent = this.Parent;
            page.State = this.State;
            return page;
        }

        public abstract Task<FileWizardPageBase> GetNextPage();
    }
}
