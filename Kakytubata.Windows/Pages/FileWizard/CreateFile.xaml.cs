﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Pages.FileWizard
{
    /// <summary>
    /// CreateFile.xaml の相互作用ロジック
    /// </summary>
    public partial class CreateFile : FileWizardPageBase
    {
        ViewModel.FileWizard.FileInitialize Data { get; set; }

        public CreateFile()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Parent.NextButton.IsEnabled = false;
            Data = new ViewModel.FileWizard.FileInitialize(State);
            DataContext = Data;
        }

#pragma warning disable CS1998 
        public override async Task<FileWizardPageBase> GetNextPage()
        {
            Data.Save();
            await State.CurrentLibrary.SaveAsync();
            return Copy(new Import(Data.CurrentFile));
        }
#pragma warning restore CS1998

        private void ImageButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Title = "Select File's Art.";
            ofd.Filter = "Supported File Formats|*.jpg;*.jpeg;*.png;*.bmp;*.gif";
            if (ofd.ShowDialog() == DialogResult.OK)
                Data.ImageFilePath = ofd.FileName;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Parent.NextButton.IsEnabled = !string.IsNullOrEmpty(TitleTextBox.Text);
        }
    }
}
