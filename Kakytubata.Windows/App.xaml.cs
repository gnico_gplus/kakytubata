﻿using EmergenceGuardian.FFmpeg;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Kakytubata.Windows
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private async void Application_Startup(object sender, StartupEventArgs e)
        {
            FFmpegConfig.FFmpegPath = Path.Combine(Path.GetDirectoryName(typeof(App).Assembly.Location), Environment.Is64BitProcess ? "ffmpeg_x64" : "ffmpeg_x86", "ffmpeg.exe");

            State.State state = new State.State() { Setting = new Settings.Setting() };
            bool IsShowWizard = true;

            void InError(bool IsRecreateAndStart)
            {
                if (IsRecreateAndStart)
                {
                    IsShowWizard = IsRecreateAndStart;
                }
                else
                {
                    Current.Shutdown();
                }
            }

            try
            {
                IsShowWizard = !(await state.LoadSettingAsync());
                if(state.Setting.LibrariesPath.Count == 0)
                {
                    InError(MessageBox.Show($"The setting file has no Library Data.\nDo you want to recreate setting?", "Setting Load Error", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes);
                }
                else
                {
                    await state.SelectCurrentLibraryAsync(0);
                }
            }
            catch (System.IO.FileNotFoundException ex)
            {
                InError(MessageBox.Show($"Couldn't load the setting file.\nPlease check if it is collect about permissions, file systems errors.\nPath:{ex.FileName}\nError Message:\n{ex.Message}\n\nDo you want to recreate setting file?", "Setting Load Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes);
            }
            catch (System.Security.SecurityException ex)
            {
                try
                {
                    InError(MessageBox.Show($"Couldn't load the setting file.\nPlease check if permissions of it is collect.\n Path:{Settings.Setting.SettingFilePath}\nError Message:\n{ex.Message}\n\nDo you want to recreate setting file?", "Setting Load Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes);
                }
                catch (System.IO.IOException)
                {
                    InError(MessageBox.Show($"Coludn't create the setting directory.\nPlease check if this program is able to create or modify %APPDATA%.\n\nDo you want to recreate setting file?", "Setting File Creation Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes);
                }
            }
            catch (System.FormatException ex)
            {
                InError(MessageBox.Show($"Oops! Couldn't load the setting file.\nError Message:\n{ex.Message}\n\nDo you want to recreate setting file?", "Setting Load Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes);
            }

            MainWindow = IsShowWizard ? (Window)(new Windows.LibraryWizardWindow(state)) : (Window)(new Windows.MainWindow(state));
            MainWindow.Show();
        }
    }
}
