﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kakytubata.Windows.Controls
{
    /// <summary>
    /// Playlist.xaml の相互作用ロジック
    /// </summary>
    public partial class Playlist : UserControl
    {
        public Playlist()
        {
            InitializeComponent();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            State.PlaylistData data = this.DataContext as State.PlaylistData;
            if (data != null && data.Parent != null)
            {
                if(data.IsCurrent)
                {
                    if (data.Parent.Parent == null) return;
                    data.Parent.CurrentIndex--;
                    data.Parent.Parent.GotoNext();
                }
                data.Parent.Remove(data);
            }
        }

        private void UserControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            State.PlaylistData data = this.DataContext as State.PlaylistData;
            if (data != null && data.Parent != null && data.Parent.Parent != null)
            {
                data.Parent.Parent.SetPlay(data);
            }
        }
    }
}
