# Kakytubata
(Maybe,) Kakytubata is a next generation Media Player with Library.  

## System Requirements
### Windows
> Operating System : Windows 7 SP1 or 10 Fall Creators Update or Later  
> CPU              : Core 2 Solo/Duo/Quad Series or Later, AMD Bulldozer Series or Later  
> GPU              : DirectX 9 Generation GPUs  
> Free Memory      : Minimum 512MB, Recommended 1GB  
> .Net Version     : Version 4.7.1 or Later

## Known Issues
### Can't show the length of music.
When you play music, length(time) must be shown right side of seek bar. But it can't show, I think it is occuerred from libVLC or Vlc.Net.  
### Play/Pause button shows wrong icons.
When you play first music of playlist, Play/Pause button shows the play icon. But it's mistake. It's under investigation.  
### Music encoding doesn't show progress of encoding.
When you encode musics, controls of music items don't show progress of encoding. I think it is occuerred from ffmpeg or ffmpeg.net.

## Used Libraries
### Opus Codec
Opus is a totally open, royalty-free, highly versatile audio codec.  
IETF Copyright © 2011-2018  

### Matroska Media Container
The extensible, open source, open standard Multimedia container.  
Copyright ® 2005 - 2016 Matroska (non-profit org), All Rights Reserved.   

### FFmpeg
A complete, cross-platform solution to record, convert and stream audio and video.   
FFmpeg Project 

### EmergenceGuardian.FFmpeg
.NET Wrapper for FFmpeg  
Etienne Charland, mysteryx93  

### VLC Media Player/libvlc
VLC media player, the best Open Source player.  
VideoLAN 2001-2018  

### VLC.DotNet.Core
.Net API to use the audio/video capabilities of the VLC libraries.  
ZeBobo5  

### taglib-sharp-netstandard2.0
A library for reading and writing audio metatags. Fork to support .NetStandard 2.0 was created by Peter Hagen : https://github.com/mono/taglib-sharp/issues/60  
Mono Project and TagLib Project, taglib-sharp  

### System.Reactive
Reactive Extensions (Rx) Main Library combining the interfaces, core, LINQ, and platform services libraries.  
.Net Foundation and Contributors

### ReactiveProperty
ReactiveProperty is MVVM and Asynchronous Extensions for Reactive Extensions(System.Reactive).  
neuecc, xin9le, okazuki  

### System.Data.SQLite.Core
The official SQLite database engine for both x86 and x64 along with the ADO.NET provider.   
SQLite Development Team  