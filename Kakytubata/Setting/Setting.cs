﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kakytubata.Settings
{
    [XmlRoot]
    public class Setting
    {
        [XmlIgnore]
        public static readonly string AppDataSettingFileLocationDirectoryName = "Kakytubata";

        [XmlIgnore]
        public static readonly string AppDataSettingFileName = "Setting.xml";

        [XmlElement]
        public List<string> LibrariesPath { get; set; }

        [XmlElement]
        public int SettingVer { get; set; }

        public static Setting CreateNew()
            => new Setting() { LibrariesPath = new List<string>(), SettingVer = 0};

        public static string SettingFilePath {
            get {
#if WINDOWS
                string AppDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string AppDataSettingFileLocation = Path.Combine(AppDataPath, AppDataSettingFileLocationDirectoryName);
                if (!Directory.Exists(AppDataSettingFileLocation))
                    Directory.CreateDirectory(AppDataSettingFileLocation);
                return Path.Combine(AppDataSettingFileLocation, AppDataSettingFileName);
#endif
            }
        }

        public static (bool IsOpened, Setting setting) Load()
        {
            string filePath = SettingFilePath;
            if (!File.Exists(filePath))
            {
                Setting result = Setting.CreateNew();
                return (false, result);
            }
            else
            {
                Setting s = null;
                XmlSerializer serializer = new XmlSerializer(typeof(Setting));
                using (StreamReader sr = new StreamReader(filePath))
                {
                    object o = serializer.Deserialize(sr);
                    if (o != null)
                        s = (Setting)o;
                }
                return s == null ? (false, Setting.CreateNew()) : (true, s);
            }
        }

        public static Task<(bool IsOpened, Setting Setting)> LoadAsync()
            => Task.Run((Func<(bool, Setting)>)Load);

        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Setting));
            using (StreamWriter sw = new StreamWriter(SettingFilePath))
            {
                serializer.Serialize(sw, this);
            }
        }

        public Task SaveAsync() => Task.Run((Action)Save);
    }
}
