﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;

namespace Kakytubata.DBManager
{
    public class DatabaseManager : IDisposable
    {
        private SQLiteConnection connection;
        private SQLiteCommand command;

        public static async Task<DatabaseManager> CreateNewAsync(string path)
        {
            DatabaseManager result = new DatabaseManager();
            SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder()
            {
                DataSource = path,
                Version = 3,
                LegacyFormat = false,
                SyncMode = SynchronizationModes.Off,
                UseUTF16Encoding = true,
                JournalMode = SQLiteJournalModeEnum.Delete
            };

            result.connection = new SQLiteConnection(builder.ToString());
            await result.connection.OpenAsync();
            result.command = result.connection.CreateCommand();
            result.command.CommandText = "CREATE TABLE file(id INTEGER PRIMARY KEY, title TEXT NOT NULL, albumID INTEGER NOT NULL, path TEXT NOT NULL)";
            await result.command.ExecuteNonQueryAsync();
            result.command.CommandText = "CREATE TABLE media(id INTEGER PRIMARY KEY, title TEXT NOT NULL, path TEXT NOT NULL, creator TEXT NOT NULL)";
            await result.command.ExecuteNonQueryAsync();
            return result;
        }
        public static DatabaseManager CreateNew(string path)
        {
            DatabaseManager result = new DatabaseManager();
            SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder()
            {
                DataSource = path,
                Version = 3,
                LegacyFormat = false,
                SyncMode = SynchronizationModes.Off,
                UseUTF16Encoding = true,
                JournalMode = SQLiteJournalModeEnum.Delete
            };

            result.connection = new SQLiteConnection(builder.ToString());
            result.connection.Open();
            result.command = result.connection.CreateCommand();
            result.command.CommandText = "CREATE TABLE media(id INTEGER PRIMARY KEY, title TEXT NOT NULL, genre INTEGER, fileID INTEGER NOT NULL, path TEXT NOT NULL)";
            result.command.ExecuteNonQuery();
            result.command.CommandText = "CREATE TABLE file(id INTEGER PRIMARY KEY, title TEXT NOT NULL, path TEXT NOT NULL)";
            result.command.ExecuteNonQuery();
            return result;
        }

        public void Close()
        {
            command?.Dispose();
            connection?.Close();
        }

        public void Dispose()
        {
            command?.Dispose();
            connection?.Dispose();
        }
    }
}
