﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Diagnostics;
using Vlc.DotNet.Wpf;
using System.IO;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Kakytubata.State
{
    public class State
    {
        public Settings.Setting Setting { get; set; } = null;

        public Library.Library CurrentLibrary { get; set; } = null;

        public State() { }

        public async Task<bool> LoadSettingAsync()
        {
            var settingResult = await Settings.Setting.LoadAsync();
            Setting = settingResult.Setting;
            return settingResult.IsOpened;
        }

        public bool LoadSetting()
        {
            var settingResult = Settings.Setting.Load();
            Setting = settingResult.setting;
            return settingResult.IsOpened;
        }

        public async Task SelectCurrentLibraryAsync(int index)
            => CurrentLibrary = await Library.Library.LoadAsync(Setting.LibrariesPath[index]);

        public void SelectCurrentLibrary(int index)
            => CurrentLibrary = Library.Library.Load(Setting.LibrariesPath[index]);
    }

    public class PlayState : State, IDisposable
    {
        public Vlc.DotNet.Core.VlcMediaPlayer VLC { get; set; }
        public Dispatcher MainWindowDispatcher { get; set; }

        public Playlist MediaList { get; }

        public PlayState(State state, VlcControl vlc, Dispatcher dispatch) : this(state, vlc, dispatch, new Playlist()) { }

        public PlayState(State state, VlcControl vlc, Dispatcher dispatch, Playlist MediaListReference)
        {
            MediaList = MediaListReference;
            MediaList.Parent = this;

            // Copy
            Setting = state.Setting;
            CurrentLibrary = state.CurrentLibrary;
            string libVLC_path = Path.Combine(Path.GetDirectoryName(typeof(PlayState).Assembly.Location), Environment.Is64BitProcess ? "libvlc_x64" : "libvlc_x86");

            MainWindowDispatcher = dispatch;

            vlc.SourceProvider.CreatePlayer(new DirectoryInfo(libVLC_path));
            VLC = vlc.SourceProvider.MediaPlayer;
            VLC.EndReached += (ss, ee) =>
            {
                MainWindowDispatcher.BeginInvoke((Action)(() =>
                {
                    if (!MediaList.IsIncrementable)
                        return;
                    MediaList.CurrentIndex++;
                    FileInfo fi = new FileInfo(MediaList[MediaList.CurrentIndex].Path);
                    VLC.SetMedia(fi);
                    VLC.Play();
                    MediaList.RemoveAt(0);
                }));
            };
            VLC.EncounteredError += (ss, ee) =>
            {
                Debug.WriteLine("An Error Occured");
            };
        }

        public void AddPlaylist(string path)
        {
            MediaList.Add(new PlaylistData(path));
            if (!VLC.IsPlaying())
            {
                MediaList.CurrentIndex = 0;
                FileInfo fi = new FileInfo(MediaList[0].Path);
                VLC.SetMedia(fi);
                VLC.Play();
            }
        }

        public void GotoNext()
        {
            VLC.Stop();
            MediaList[MediaList.CurrentIndex].IsCurrent = false;
            if (!MediaList.IsIncrementable)
                return;
            MediaList.CurrentIndex++;
            FileInfo fi = new FileInfo(MediaList[MediaList.CurrentIndex].Path);
            VLC.SetMedia(fi);
            VLC.Play();
        }

        public void PlaylistStopAndClear()
        {
            if (VLC.IsPlaying())
                VLC.Stop();
            MediaList.Clear();
        }

        public void PlaylistClear()
        {
            MediaList.Clear();
        }

        public void SetPlay(PlaylistData pdata)
        {
            if (!MediaList.Contains(pdata)) return;
            MediaList.CurrentItem = pdata;
            VLC.Stop();
            var fi = new FileInfo(pdata.Path);
            VLC.SetMedia(fi);
            VLC.Play();
        }

        public void Dispose()
        {
            try
            {
                VLC?.Dispose();
            }
            catch
            {
                Environment.Exit(0);
            }
        }
    }
}
