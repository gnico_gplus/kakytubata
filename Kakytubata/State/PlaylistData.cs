﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Kakytubata.State
{
    public class PlaylistData : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private string _Name;
        public string Name {
            get => _Name;
            set {
                _Name = value;
                RaisePropertyChanged();
            }
        }

        private string _Path;
        public string Path {
            get => _Path;
            set {
                _Path = value;
                RaisePropertyChanged();
            }
        }

        private bool _IsCurrent;
        public bool IsCurrent {
            get => _IsCurrent;
            set {
                _IsCurrent = value;
                RaisePropertyChanged();
            }
        }

        internal Playlist Parent = null;

        public PlaylistData(string path) : this(path, System.IO.Path.GetFileName(path)) { }

        public PlaylistData(string path, string name)
        {
            _Path = path;
            _Name = name;
            _IsCurrent = false;
        }
    }

    public class Playlist : ObservableCollection<PlaylistData>
    {
        public PlayState Parent { get; set; } = null;

        public int _CurrentIndex = -1;
        public int CurrentIndex {
            get => _CurrentIndex;
            set {
                if(_CurrentIndex >= 0 && _CurrentIndex < Count)
                {
                    this[_CurrentIndex].IsCurrent = false;
                }
                if (value < 0 || value >= Count)
                    return;
                _CurrentIndex = value;
                this[_CurrentIndex].IsCurrent = true;
            }
        }

        public PlaylistData CurrentItem {
            get => CurrentIndex < 0 || CurrentIndex >= Count ? null : this[CurrentIndex];
            set {
                int n = -1;
                for (int i = 0; i < Count; ++i)
                    if (object.ReferenceEquals(value, this[i])) n = i;
                if (n == -1) return;
                this[CurrentIndex].IsCurrent = false;
                CurrentIndex = n;
                this[CurrentIndex].IsCurrent = true;
            }
        }

        public bool IsIncrementable =>_CurrentIndex >= 0 && Count > (_CurrentIndex + 1);

        protected override void InsertItem(int index, PlaylistData item)
        {
            item.Parent = this;
            base.InsertItem(index, item);
        }
        protected override void SetItem(int index, PlaylistData item)
        {
            item.Parent = this;
            base.SetItem(index, item);
        }
    }
}
