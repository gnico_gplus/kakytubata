﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reactive;
using System.Reactive.Linq;
using System.IO;
using MimeTypes = System.Net.Mime.MediaTypeNames.Image;
#if WINDOWS
using System.Windows.Media.Imaging;
#endif

namespace Kakytubata.Library
{
    public static class MediaTag
    {
        public static Task<TagLib.File> OpenFileAsync(string path)
            => Task.Run(() => TagLib.File.Create(path));

#if WINDOWS
        public static Task<(TagLib.File, IObservable<(TagLib.PictureType, BitmapSource)>)> OpenFileWithPicture(string path)
        {
            return Task.Run(() =>
            {
                TagLib.File file = TagLib.File.Create(path);
                var obs = Observable.Create<(TagLib.PictureType, BitmapSource)>(async (observer, ct) =>
                {
                    foreach(var pic in file.Tag.Pictures)
                    {
                        if (ct.IsCancellationRequested) break;
                        try
                        {
                            using (var mem = new MemoryStream(pic.Data.Data))
                            {
                                BitmapSource source = null;
                                BitmapDecoder decoder = null;
                                await Task.Run(() =>
                                {
                                    switch (pic.MimeType)
                                    {
                                        case MimeTypes.Jpeg:
                                            decoder = new JpegBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                                            break;
                                        case "image/bmp":
                                            decoder = new BmpBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                                            break;
                                        case "image/png":
                                        case "image/x-png":
                                            decoder = new PngBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                                            break;
                                        default:
                                            return;
                                    }
                                    source = decoder.Frames[0];
                                });
                                if(source != null)
                                    observer.OnNext((pic.Type, source));
                            }
                        }
                        catch(Exception ex)
                        {
                            observer.OnError(ex);
                        }
                    }
                    observer.OnCompleted();
                });
                return (file, obs);
            });
        }

        public static Task<(TagLib.File, IObservable<(TagLib.PictureType, BitmapSource)>)> OpenPictures(TagLib.File file)
        {
            return Task.Run(() =>
            {
                var obs = Observable.Create<(TagLib.PictureType, BitmapSource)>(async (observer, ct) =>
                {
                    foreach (var pic in file.Tag.Pictures)
                    {
                        if (ct.IsCancellationRequested) break;
                        try
                        {
                            using (var mem = new MemoryStream(pic.Data.Data))
                            {
                                BitmapSource source = null;
                                BitmapDecoder decoder = null;
                                await Task.Run(() =>
                                {
                                    switch (pic.MimeType)
                                    {
                                        case MimeTypes.Jpeg:
                                            decoder = new JpegBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                                            break;
                                        case "image/bmp":
                                            decoder = new BmpBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                                            break;
                                        case "image/png":
                                        case "image/x-png":
                                            decoder = new PngBitmapDecoder(mem, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnLoad);
                                            break;
                                        default:
                                            return;
                                    }
                                    source = decoder.Frames[0];
                                });
                                if (source != null)
                                    observer.OnNext((pic.Type, source));
                            }
                        }
                        catch (Exception ex)
                        {
                            observer.OnError(ex);
                        }
                    }
                    observer.OnCompleted();
                });
                return (file, obs);
            });
        }
#endif
    }
}
