﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Kakytubata.DBManager;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Kakytubata.Library
{ 
    [XmlRoot]
    public class Library : MVVM.PropertyChangedBase
    {
        public static readonly string LibraryDataFileName = "Library.xml";
        public static readonly string DataBaseFileName = "Library.db";

        private string title = "";
        [XmlElement(ElementName = "Title")]
        public string Title {
            get => title;
            set {
                title = value;
                Notify();
            }
        }

        private string libraryPath = "";
        [XmlIgnore]
        public string LibraryPath {
            get => libraryPath;
            set {
                libraryPath = value;
                Notify();
            }
        }

        private MediaCodec.FileType compressor = MediaCodec.FileType.FLAC;

        [XmlElement(ElementName = "Compressor")]
        public MediaCodec.FileType Compressor {
            get => compressor;
            set {
                compressor = value;
                Notify();
            }
        }

        [XmlIgnore]
        public DatabaseManager DataBase { get; set; } = null;

        [XmlElement]
        public ObservableCollection<FileInfo> Files { get; private set; } = new ObservableCollection<FileInfo>();

        public Task BeginFilesLoad(Dispatcher disp)
        {
            for (int i = 0; i < Files.Count; ++i)
                Files[i].Parent = this;
            return Task.Run(() =>
            {
                for(int i = 0; i < Files.Count; ++i)
                {
                    FileInfo current = Files[i];
                    var file = current.Load();
                    if (file == null) continue;
                    disp.BeginInvoke((Action)(() => current.Data = file));
                }
            });
        }

        /// <summary>
        /// Initialize directory of Library.
        /// </summary>
        public async Task InitializeDirectoryAsync()
        {
            if(Directory.Exists(LibraryPath))
            {
                Directory.Delete(LibraryPath, true);
            }
            Directory.CreateDirectory(LibraryPath);
            string dataPath = Path.Combine(LibraryPath, LibraryDataFileName);
            await Task.Run(() =>
            {
                using (StreamWriter sw = new StreamWriter(dataPath, false))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Library));
                    serializer.Serialize(sw, this);
                }
            });
            dataPath = Path.Combine(LibraryPath, DataBaseFileName);
            DataBase = await DatabaseManager.CreateNewAsync(dataPath);
        }

        public async Task SaveAsync()
        {
            string dataPath = Path.Combine(LibraryPath, LibraryDataFileName);
            await Task.Run(() =>
            {
                using (StreamWriter sw = new StreamWriter(dataPath, false))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Library));
                    serializer.Serialize(sw, this);
                }
            });
        }

        /// <summary>
        /// Load the library.
        /// </summary>
        /// <param name="path">The directory path of the library.</param>
        public static Library Load(string path)
        {
            string dataPath = Path.Combine(path, LibraryDataFileName);
            Library result = null;
            using (StreamReader sr = new StreamReader(dataPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Library));
                result = (Library)serializer.Deserialize(sr);
            }
            result.LibraryPath = path;
            return result;
        }

        /// <summary>
        /// Load the library.
        /// </summary>
        /// <param name="path">The directory path of the library.</param>
        public static Task<Library> LoadAsync(string path) => Task.Run(() => Load(path));
    }

}
