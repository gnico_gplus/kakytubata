﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Kakytubata.Library
{
    [XmlRoot]
    public class MediaInfo : MVVM.PropertyChangedBase
    {
        [XmlIgnore]
        private TagLib.File data = null;
        [XmlIgnore]
        public TagLib.File Data {
            get => data;
            set {
                data = value;
                Notify();
                Notify("IsLoaded");
            }
        }
    
        [XmlIgnore]
        private TagLib.Tag tag = null;
        [XmlIgnore]
        public TagLib.Tag TagData {
            get => tag;
            set {
                tag = value;
                Notify();
                Notify("Title");
            }
        }

        [XmlElement]
        public string FileName { get; set; }

        [XmlIgnore]
        public bool IsLoaded => data != null;

        [XmlIgnore]
        public string Title => TagData == null || string.IsNullOrEmpty(TagData.Title) ? FileName : TagData.Title;

#if WINDOWS
        [XmlIgnore]
        public Windows.Pages.MainWindow.FilePage ParentPage = null;
#endif
    }
}
