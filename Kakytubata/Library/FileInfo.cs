﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace Kakytubata.Library
{
    [XmlRoot]
    public class FileInfo : MVVM.PropertyChangedBase
    {
        [XmlIgnore]
        private File data;
        [XmlIgnore]
        public File Data {
            get => data;
            set {
                data = value;
                Notify();
                Notify("IsLoaded");
                Notify("ImagePath");
            }
        }

        [XmlElement]
        public string DirectoryName { get; set; }

        [XmlIgnore]
        public bool IsLoaded => Data != null;

        [XmlIgnore]
        private Library parent;
        [XmlIgnore]
        public Library Parent {
            get => parent;
            set {
                parent = value;
                Notify("ImagePath");
            }
        }

        [XmlIgnore]
        public string ImagePath {
            get {
                if (Data == null || Parent == null || string.IsNullOrEmpty(Data.ImageFileName))
                    return null;
                return Path.Combine(Parent.LibraryPath,DirectoryName, Data.ImageFileName);
            }
        }

        public string FullPath {
            get {
                if (data == null || parent == null) return null;
                return Path.Combine(Parent.LibraryPath, DirectoryName);
            }
        }

        public FileInfo() { }

        public FileInfo(Library parent)
        {
            Parent = parent;
        }

        public string InitializeBaseDirectoryName(string directoryName = null)
        {
            if (data == null) return null;
            if (directoryName == null) DirectoryName = Data.Title;
            else DirectoryName = directoryName;
            return FullPath;
        }

        public void InitializeDirectory(string directoryName = null)
        {
            Directory.CreateDirectory(FullPath);
        }

        public void Save()
        {
            Data.Save(FullPath);
        }

        public File Load()
        {
            if (IsLoaded) return null;
            return File.Load(Path.Combine(Parent.LibraryPath, DirectoryName));
        }
        public void LoadAndSet()
        {
            if (IsLoaded) return;
            Data = File.Load(Path.Combine(Parent.LibraryPath, DirectoryName));
        }

        public (Task, CancellationTokenSource) StartMediaLoad(Dispatcher dispatch)
        {
            if (data == null) return (null, null);
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            return (Task.Run(() =>
            {
                var ms = Data.Media;
                for(int i = 0;i < ms.Count; ++i)
                {
                    var m = ms[i];
                    if (m.IsLoaded) continue;
                    TagLib.File f = null;
                    try
                    {
                        f = TagLib.File.Create(Path.Combine(FullPath, m.FileName));

                        TagLib.Tag t = f.Tag;
                        dispatch.BeginInvoke((Action)(() =>
                        {
                            m.Data = f;
                            m.TagData = t;
                        }));
                    }
                    catch { }
                    if (token.IsCancellationRequested) return;
                }
            }), tokenSource);
        }

#if WINDOWS
        public void SetParentPage(Windows.Pages.MainWindow.FilePage page)
        {
            for (int i = 0; i < Data.Media.Count; ++i)
                Data.Media[i].ParentPage = page;
        }

        public void ReleaseParentPage()
        {
            for (int i = 0; i < Data.Media.Count; ++i)
                Data.Media[i].ParentPage = null;
        }
#endif
    }
}
