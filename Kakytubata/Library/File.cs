﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Collections.ObjectModel;

namespace Kakytubata.Library
{
    [XmlRoot]
    public class File : MVVM.PropertyChangedBase
    {
        private static string DataFileName => "FileInfo.xml";
        public static string DefaultArtFileName => "FileArt";

        [XmlIgnore]
        private string title;
        [XmlElement]
        public string Title {
            get => title;
            set {
                title = value;
                Notify();
            }
        }

        [XmlElement]
        public MVVM.ObservableDictionary<string, string> Creator { get; set; } = new MVVM.ObservableDictionary<string, string>();

        [XmlElement]
        public ObservableCollection<MediaInfo> Media { get; set; } = new ObservableCollection<MediaInfo>();

        [XmlElement]
        public string ImageFileName { get; set; }

        public static File CreateEmpty()
            => new File() { Title = "", Creator = new MVVM.ObservableDictionary<string, string>(), ImageFileName = "" };

        public void Save(string parentPath)
        {
            string dataPath = Path.Combine(parentPath, DataFileName);
            using(StreamWriter sw = new StreamWriter(dataPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(File));
                serializer.Serialize(sw, this);
            }
        }

        public static File Load(string parentPath)
        {
            File result;
            string dataPath = Path.Combine(parentPath, DataFileName);
            using(StreamReader sr = new StreamReader(dataPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(File));
                result = (File)serializer.Deserialize(sr);
            }
            return result;
        }
    }
}
