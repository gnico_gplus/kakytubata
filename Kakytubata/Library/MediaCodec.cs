﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Kakytubata.Library
{
    public static class MediaCodec
    {
        /// <summary>
        /// 特定の属性を取得する
        /// (from https://qiita.com/soi/items/b79c376e49a252439940)
        /// </summary>
        /// <typeparam name="TAttribute">属性型</typeparam>
        public static TAttribute GetAttribute<TAttribute>(this Enum value) where TAttribute : Attribute
        {
            //リフレクションを用いて列挙体の型から情報を取得
            var fieldInfo = value.GetType().GetField(value.ToString());
            //指定した属性のリスト
            var attributes
                = fieldInfo.GetCustomAttributes(typeof(TAttribute), false)
                .Cast<TAttribute>();
            //属性がなかった場合、空を返す
            if ((attributes?.Count() ?? 0) <= 0)
                return null;
            //同じ属性が複数含まれていても、最初のみ返す
            return attributes.First();
        }

        public enum FileType
        {
            [FileSize(0)]
            [CodecName("flac")]
            [Description("Lossless (FLAC)")]
            FLAC = 0,
            [FileSize(256)]
            [CodecName("libopus")]
            [Description("Best (Opus 256kbps)")]
            Opus256 = 1,
            [FileSize(128)]
            [CodecName("libopus")]
            [Description("Good (Opus 128kbps)")]
            Opus128 = 2,
            [FileSize(96)]
            [CodecName("libopus")]
            [Description("Normal (Opus 96kbps)")]
            Opus96 = 3,
            [FileSize(24)]
            [CodecName("libopus")]
            [Description("Highly Compressed (Opus 24kbps)")]
            Opus24 = 4
        }

        public class FileSizeAttribute : Attribute
        {
            public int Kbps { get; set; }
            public FileSizeAttribute(int kbps)
            {
                Kbps = kbps;
            }
        }

        public class CodecNameAttribute : Attribute
        {
            public string Name { get; set; }
            public CodecNameAttribute(string name)
            {
                Name = name;
            }
        }
    }
}
